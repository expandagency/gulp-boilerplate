var gulp = require("gulp");
var sass = require("gulp-sass");
var plumber = require("gulp-plumber");
var postcss = require("gulp-postcss");
var autoprefixer = require("autoprefixer");
var cssnano = require("cssnano");
var concat = require("gulp-concat");
var uglify = require('gulp-uglify');
var sourcemaps = require("gulp-sourcemaps");


// :: PATHS
// ------------------------------------------------
var paths = {

    scss: {
        src: [
            './assets/scss/**/*.scss'
        ],

        // Output
        dest: './css'
    },

    scripts: {
        src: [
            // Libs
    		// 'bower_components/jquery/dist/jquery.min.js',

            // All other *js
            'assets/js/**/*.js'
        ],

        // Output
        dest: './js'
    }
};


// :: TASK - [styles]
// ------------------------------------------------
function styles() {
    return (
        gulp
            .src(paths.scss.src)
            .pipe(plumber({
                errorHandler: function (err) {
                  console.log(err);
                  this.emit('end');
                }
        	  }))
            .pipe(sourcemaps.init())
            .pipe(sass())
            .on("error", sass.logError)
            // Use postcss with autoprefixer and compress the compiled file using cssnano
            .pipe(postcss([autoprefixer(), cssnano()]))
            // Now add/write the sourcemaps
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(paths.scss.dest))
    );
}



// :: TASK - [scripts]
// ------------------------------------------------
function scripts() {
  return (
    gulp
      .src(paths.scripts.src)
      .pipe(concat('scripts.min.js'))
      .pipe(plumber({
  	      errorHandler: function (err) {
            console.log(err);
            this.emit('end');
  	      }
  	  }))
      .pipe(uglify())
      .pipe(gulp.dest(paths.scripts.dest))
  );
}



// :: TASK - [watch]
// ------------------------------------------------
function watch() {

  gulp.watch(paths.scss.src, styles);
  gulp.watch(paths.scripts.src, scripts);

}

// Run our 'styles' and 'scripts' tasks first - then 'watch' task which stay running
var build = gulp.series(gulp.parallel(styles, scripts), watch);

// Export Tasks
// exports.styles = styles;
// exports.watch = watch
exports.default = build;
